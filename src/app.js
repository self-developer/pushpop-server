import React, { Component } from 'react'
import axios from 'axios';
import Pusher from 'pusher-js';

import './styles';
import "./style.css";

import InputBox from './component/InputBox';
import MessageList from './component/MessageList';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            username: '',
            chats: [
                {'id':'2','message':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','username':'selfquery','provider':'web','timestamp':'04/20/2020 9:30am'},
                {'id':'3','message':'hello world','username':'selfquery','provider':'youtube','timestamp':'04/20/2020 9:30am'},
                {'id':'2','message':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','username':'selfquery','provider':'discord','timestamp':'04/20/2020 9:30am'},
                {'id':'2','message':'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','username':'selfquery','provider':'twitch','timestamp':'04/20/2020 9:30am'},
                ]
        };
    }

    componentDidMount() {
        const username = window.prompt('Username: ', 'Anonymous');
        this.setState({ username });
        const pusher = new Pusher('719652eeb3db5b245ce5', {
            cluster: 'us3',
            forceTLS: true
        });
        const channel = pusher.subscribe('chat');
        channel.bind('message', data => {
            this.setState({ chats: [data, ...this.state.chats], test: '' });
        });
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    handleTextChange(e) {
        if (e.keyCode === 13 && this.state.text !== '') {
            if (this.state.username === '') {
                alert('username required to post.')
                return;
            }
            const payload = {
                username: this.state.username,
                message: this.state.text,
                provider: 'web'
            };
            axios.post('/message', payload);
            this.setState({ text: '' });
        }
        else {
            this.setState({ text: e.target.value });
        }
    }

    render() {
        return (
            <div>
                
                <div className="chat-window big-hidden">
                    <InputBox
                        text={this.state.text}
                        username={this.state.username}
                        handleTextChange={this.handleTextChange}
                    />
                    <MessageList chats={this.state.chats} />
                </div>
                
            </div>
        )
    }
}

export default App;
