import React from 'react'

export default ({ text, username, handleTextChange }) => (
    <div className="fixed-input">
        <input
            type="text"
            value={text}
            placeholder="chat here..."
            className="form-control"
            onChange={handleTextChange}
            onKeyDown={handleTextChange}
            maxlength="150"
        />
    </div>
);