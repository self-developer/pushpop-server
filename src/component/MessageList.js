import React from "react";

export default ({ chats }) => (
    <ul className="msg-box-list">
    {chats.map(chat => {
        return (
            <div key={chat.id} id={chat.provider} className="row msg-box">
                <div className="msg-div">
                    <span className="username"><strong>{chat.username}</strong></span>
                    <span className="provider"> from <em>{chat.provider}</em></span>
                    <span className="timestamp"> <em>{chat.timestamp}</em></span>
                    <p className="msg">{chat.message}</p>
                </div>
            </div>
        );
    })}
  </ul>
);
